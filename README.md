# zitec/emagmarketplace

The eMAG Marketplace module helps you integrate your Magento 2 store with the eMAG Marketplace
vendor platform. This will allow your store to:
* Automatically send product documentation (titles, descriptions, images, etc.), product price info
and product stock info, to the eMAG Marketplace platform.
* Automatically send new products and any updates for previously sent products.
* Automatically receive orders from the eMAG Marketplace platform and import them in your
Magento store.
* Automatically send order updates to the eMAG Marketplace platform.
* Upload an invoice to eMAG Marketplace
* Generate AWB’s for imported eMAG orders

---

## Requirements

* PHP >= 7.0
* composer

---

## Instalation

Run the following series of command (from root of your project):

```
composer config repositories.zitec git https://bitbucket.org/gymbeamdev/zitec-emagmarketplace.git
composer require zitec/emagmarketplace:1.0.2
```

---